set nu
set tabstop=4
set shiftwidth=4

syntax enable
set background=dark
"colorscheme solarized
let g:solarized_termcolors=256
if has('gui_running')
	set background=light
else
	set background=dark
endif

" force fim to reread file after it change
set autoread

" Enable swap directory in a local folder (avoids ghost copies)
set directory=$HOME/.vim/swap//

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

set clipboard+=unnamedplus

"" Pathogen config
"execute pathogen#infect()
"filetype plugin indent on
