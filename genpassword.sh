#! /bin/sh
#
# genpassword.sh
#
# Distributed under terms of the GPLv3 license.
#

help() {
	echo "genpasswd.sh: A Password Generator\n
	Usage: genpasswd.sh [number]\n"
}

if [[ "$1" == "" || "$1" == "-h" || "$1" == "--help" ]]; then
	help
	exit 0
else
	cat /dev/urandom | base64 | tr -dc 'a-zA-Z0-9' | fold -w $1 | head -n 1
fi
